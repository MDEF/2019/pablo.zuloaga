#include <Arduino.h>

int data;                               // the reading from the analog input

int sensor = A0;


void setup() {

  pinMode(sensor, INPUT); //initialize ldr sensor as INPUT
  Serial.begin(9600); //begin the serial monitor at 9600 baud

}

void loop() {

  data = analogRead(sensor);
  Serial.print("% Humedad = ");
  Serial.println(data);
}