#include <Arduino.h>

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(D8, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(D8, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(D8, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(D6, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(D5, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D8, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(D8, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(D6, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(D5, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D8, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(200);                       // wait for a second
  digitalWrite(D8, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(200);                       // wait for a second
  digitalWrite(D6, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(200);                       // wait for a second
  digitalWrite(D5, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D8, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(D8, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(D6, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(D5, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D8, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(50);                       // wait for a second
  digitalWrite(D8, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(50);                       // wait for a second
  digitalWrite(D6, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(50);                       // wait for a second
  digitalWrite(D5, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D8, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(30);                       // wait for a second
  digitalWrite(D8, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(30);                       // wait for a second
  digitalWrite(D6, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(30);                       // wait for a second
  digitalWrite(D5, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D8, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(20);                       // wait for a second
  digitalWrite(D8, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(20);                       // wait for a second
  digitalWrite(D6, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(20);                       // wait for a second
  digitalWrite(D5, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D8, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(10);                       // wait for a second
  digitalWrite(D8, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(10);                       // wait for a second
  digitalWrite(D6, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D5, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(10);                       // wait for a second
  digitalWrite(D5, LOW);    // turn the LED off by making the voltage LOW
  delay(0);                       // wait for a second
  digitalWrite(D8, HIGH);    // turn the LED off by making the voltage LOW              // wait for a second
  digitalWrite(D6, HIGH);    // turn the LED off by making the voltage LOW
  digitalWrite(D5, HIGH);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
  digitalWrite(D8, LOW);    // turn the LED off by making the voltage LOW              // wait for a second
  digitalWrite(D6, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(D5, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}