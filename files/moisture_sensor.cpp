#include <Arduino.h>

int data;                               // the reading from the analog input
const int threshold = 100;                   // the threshold

int sensor = A0;
int LEDG = 8;
int LEDY = 7;
int LEDR = 6;

void setup() {
  pinMode(LEDG, OUTPUT);
  pinMode(LEDY, OUTPUT);
  pinMode(LEDR, OUTPUT);
  pinMode(sensor, INPUT); //initialize ldr sensor as INPUT
  Serial.begin(9600); //begin the serial monitor at 9600 baud

}

void loop() {

  data = analogRead(sensor);
  Serial.print("% Humedad = ");
  Serial.println(data/4);

  digitalWrite(LEDG, LOW);
  if (data >= (threshold + 200)) {
    digitalWrite(LEDG, HIGH);
    digitalWrite(LEDY, LOW);
    digitalWrite(LEDR, LOW);
  }  else if (data >= (threshold) and (data < threshold + 200)) 
  {
    digitalWrite(LEDY, HIGH);
    digitalWrite(LEDG, LOW);
    digitalWrite(LEDR, LOW);
  }
  else if (data <= (threshold)) {
    digitalWrite(LEDR, HIGH);
    digitalWrite(LEDG, LOW);
    digitalWrite(LEDY, LOW);
  }
  
  delay(500);        // delay in between reads for stability
}