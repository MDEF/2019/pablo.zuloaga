// breathe LED (on gpio 9) 6 times for 1500ms, waiting for 500ms after each run
#include <jled.h>

JLed leds[] = {
    JLed(D5).Breathe(1000).Forever(),
    JLed(D6).Breathe(2000).Forever(),
    JLed(D8).Breathe(4000).Forever()
};

JLedSequence sequence(JLedSequence::eMode::PARALLEL, leds);

void setup() {
}

void loop() {
    sequence.Update();
}